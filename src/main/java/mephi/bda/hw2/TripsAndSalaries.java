package mephi.bda.hw2;

import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.api.java.function.VoidFunction2;
import org.apache.spark.streaming.Duration;
import org.apache.spark.streaming.Time;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;

import scala.Tuple2;


public class TripsAndSalaries {
	static String STR_SCALE = "1m";

	//HDFS path with the input from MySQL
	public static String inputFilePath = "hdfs://hadoop-master:9000/hw2/input";
	//HDFS output path with the aggregated data
	public static String outFilePath = "hdfs://hadoop-master:9000/hw2/output";

	private static JavaStreamingContext sc;
	final static Logger logger = Logger.getLogger(mephi.bda.hw2.TripsAndSalaries.class);
	
	//Method for all computations
	public static void Process(JavaDStream<String> inputStream)
	{
		String outputPath = "";
		
//		Create paired data stream:
//		key: age category
//		value:
//			key:
//				key: 1 for occurrence
//				value: salary in the given month
//			value:
//				key: 1 for occurrence
//				value: number of trips in the given month
		JavaPairDStream<String, Tuple2<Tuple2<Integer, Integer>, Tuple2<Integer, Integer>>> input = inputStream.mapToPair(
				  new PairFunction<String, String, Tuple2<Tuple2<Integer, Integer>, Tuple2<Integer, Integer>>>() {
					private static final long serialVersionUID = 1L;
					@Override
					public Tuple2<String, Tuple2<Tuple2<Integer, Integer>, Tuple2<Integer, Integer>>> call(String input) throws Exception {
						int age = Integer.parseInt(input.split(",")[0]);
						int salary = Integer.parseInt(input.split(",")[1]);
						int numOfTrips = Integer.parseInt(input.split(",")[2]);
						String ageCategory;
						if (age < 20)
						 ageCategory = "<20";
						else if (age < 30)
						 ageCategory = "20-29";
						else if (age < 40)
						 ageCategory = "30-39";
						else
						 ageCategory = "40+";
						Tuple2<Integer, Integer> salaryTuple=new Tuple2<Integer, Integer>(1, salary);
						Tuple2<Integer, Integer> numOfTripsTuple=new Tuple2<Integer, Integer>(1, numOfTrips);
						Tuple2<Tuple2<Integer, Integer>, Tuple2<Integer, Integer>> value =
								new Tuple2<Tuple2<Integer, Integer>, Tuple2<Integer, Integer>>(salaryTuple, numOfTripsTuple);
						return new Tuple2<String, Tuple2<Tuple2<Integer, Integer>, Tuple2<Integer, Integer>>>(ageCategory, value);
					}
				  }
				);
		
		//Aggregate by age category, calculate total number of months and sum of salaries and trips
		JavaPairDStream<String, Tuple2<Tuple2<Integer, Integer>, Tuple2<Integer, Integer>>> reduced = input.reduceByKey(
				new Function2<
					Tuple2<Tuple2<Integer, Integer>, Tuple2<Integer, Integer>>, 
					Tuple2<Tuple2<Integer, Integer>, Tuple2<Integer, Integer>>,
					Tuple2<Tuple2<Integer, Integer>, Tuple2<Integer, Integer>>
				>() {
					private static final long serialVersionUID = 1L;

					@Override
					public Tuple2<Tuple2<Integer, Integer>, Tuple2<Integer, Integer>> call(
							Tuple2<Tuple2<Integer, Integer>, Tuple2<Integer, Integer>> v1,
							Tuple2<Tuple2<Integer, Integer>, Tuple2<Integer, Integer>> v2
					) throws Exception {
						int salCount=v1._1._1 + v2._1._1;
						int salSum=v1._1._2 + v2._1._2;
						int tripCount=v1._2._1 + v2._2._1;
						int tripSum=v1._2._2 + v2._2._2;
						Tuple2<Integer, Integer> key = new Tuple2<Integer, Integer>(salCount, salSum);
						Tuple2<Integer, Integer> value = new Tuple2<Integer, Integer>(tripCount, tripSum);
						return new Tuple2<Tuple2<Integer, Integer>, Tuple2<Integer, Integer>>(key, value);
					}
				}
		);

		//Calculate averages and map to comma-separated format
		JavaDStream<String> output = reduced.map(
				new Function<
					Tuple2<String, Tuple2<Tuple2<Integer, Integer>, Tuple2<Integer, Integer>>>,
					String
				>() {
					private static final long serialVersionUID = 1L;
					@Override
					public String call(Tuple2<String, Tuple2<Tuple2<Integer, Integer>, Tuple2<Integer, Integer>>> v1)
							throws Exception {
						return String.format("%s, %.2f, %.2f", v1._1, (float) v1._2._1._2/v1._2._1._1, (float) v1._2._2._2/v1._2._2._1);
					}
				}
		);
		//Print results to the console 
		output.print();
		//Print results to the output file in HDFS if stream is not empty
		output.foreachRDD(new VoidFunction2<JavaRDD<String>, Time>() {
			private static final long serialVersionUID = 1L;
			@Override
			public void call(JavaRDD<String> v1, Time v2) throws Exception {
				if (!v1.isEmpty())
					//v1.saveAsTextFile(outFilePath + "/output-" + v2.toString().split(" ")[0]);
					v1.saveAsTextFile(outFilePath);
			}
		});
	}

	//main method - invoked by spark-submit
	public static void main(String[] args) throws InterruptedException {
		SparkConf sparkConf = new SparkConf();
		//Initialize context, perform computation every 5 seconds
 		sc = new JavaStreamingContext(sparkConf, new Duration(5000));
 		
 		//Initialize input stream from HDFS
 		JavaDStream<String> inputStream = sc.textFileStream(inputFilePath);
 		//Process input stream
		Process(inputStream);
		//Start spark job
	    sc.start();
	    //Wait for termination of the spark job
	    sc.awaitTermination();		
	}
}
