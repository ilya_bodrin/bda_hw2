## Task:
Application that calculate average salary and abroad trips count statistics about citizens. Row input format: (passport №, month,  salary) or (passport №, month, number of abroad trips)

* Back-end: MySQL
* Import tool: Sqoop
* Storage: HDFS
* Computation tool: Spark streaming

## Deployment steps:

Clone repository:
```bash
git clone https://ilya_bodrin@bitbucket.org/ilya_bodrin/bda_hw2.git
cd bda_hw2
```

Create docker network:
```bash
docker network create hadoop
```

Pull and prepare MySQL docker image:
```bash
docker pull mysql:8.0
docker network create hadoop
docker run --hostname mysql --network hadoop --name mysql -e MYSQL_ROOT_PASSWORD=Orion123 -d mysql:8.0
docker cp scripts/mysql_import_test_data.sh mysql:/opt/
docker exec -it mysql bash
mysql -u root -p < /opt/mysql_import_test_data.sh
exit
```

Pull and prepare Hadoop docker image:
```bash
docker pull ilyabodrin/hadoop_hw2:2.0
chmod 777 start-hadoop-container.sh
./scripts/start-hadoop-container.sh
exit
```

Build Spark project:
```bash
mvn clean install
```
![picture](screenshots/mvn_build.png)


Copy jar to docker container:
```bash
docker cp target/hw2-0.0.1-SNAPSHOT.jar hadoop-master:/opt/
```

Enter hadoop container and start hadoop:
```bash
docker exec -it hadoop-master bash
. ~/.profile
./start-hadoop.sh
hadoop dfsadmin -safemode leave
```

Start spark job:
```bash
spark-submit --class mephi.bda.hw2.TripsAndSalaries --master local[2] /opt/hw2-0.0.1-SNAPSHOT.jar
```

Open new terminal, enter hadoop container and run sqoop import:
```bash
docker exec -it hadoop-master bash
hadoop fs -rm -r /hw2/input/* /hw2/sqoop /hw2/output/*
sqoop import --connect jdbc:mysql://mysql/hw2 --username root --password Orion123 --query 'select t1.age, t2.salary, t3.num_of_trips from hw2.citizens t1 inner join hw2.salaries t2 on t1.passport_num=t2.passport_num inner join hw2.trips t3 on t1.passport_num=t3.passport_num and t2.month=t3.month where $CONDITIONS' --target-dir /hw2/sqoop -m 1
hadoop fs -cp /hw2/sqoop/part* /hw2/input/
```

Check results in the terminal from which spark job has been started:
![picture](screenshots/spark_result.png)


